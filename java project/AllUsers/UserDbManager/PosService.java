package UserDbManager;

	import java.util.List;

	import javax.persistence.EntityManager;
	import javax.persistence.Persistence;
	import javax.persistence.PersistenceUnit;
	import javax.persistence.TypedQuery;

	import UserDbEntities.PositionTable;

	public class PosService {

		@PersistenceUnit(name = "UsersDataBase")
	    public EntityManager em = Persistence.createEntityManagerFactory("UsersDataBase").createEntityManager();
		 
	    public PositionTable add(PositionTable  pt){
	        em.getTransaction().begin();
	        PositionTable pFromDB = em.merge(pt);
	        em.getTransaction().commit();
	        return pFromDB;
	    }
	 
	    public void delete(long id){
	        em.getTransaction().begin();
	        em.remove(get(id));
	        em.getTransaction().commit();
	    }
	 
	    public PositionTable  get(long id){
	        return em.find(PositionTable .class, id);
	    }
	 
	    public void update(PositionTable pt){
	        em.getTransaction().begin();
	        em.merge(pt);
	        em.getTransaction().commit();
	    }
	 
	    public List<PositionTable> getAll(){
	        TypedQuery<PositionTable> namedQuery = em.createNamedQuery("PositionTable.getAll", PositionTable.class);
	        return namedQuery.getResultList();
	    }
	 
	}
