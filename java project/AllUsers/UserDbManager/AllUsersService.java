package UserDbManager;

	import javax.persistence.EntityManager;
	import javax.persistence.Persistence;
import javax.persistence.PersistenceUnit;
import javax.persistence.TypedQuery;

import UserDbEntities.AllUsersTable;
import UserDbEntities.DepTable;
import UserDbEntities.PositionTable;

import java.util.List;
	 
	public class AllUsersService {
		
		public DepTable dt;
		public DepManager dtm;
		public PositionTable pt;
		public PosService pts;
		
		@PersistenceUnit(name = "UsersDataBase")
	    public EntityManager em = Persistence.createEntityManagerFactory("UsersDataBase").createEntityManager();
		
		public AllUsersService(){
			dt = new DepTable();
			dtm = new DepManager();
			pt = new PositionTable();
			pts = new PosService();
		}
	 
	    public AllUsersTable add(AllUsersTable us){
	        em.getTransaction().begin();
	        AllUsersTable usFromDB = em.merge(us);
	        em.getTransaction().commit();
	        return usFromDB;
	    }
	 
	    public void delete(long id){
	        em.getTransaction().begin();
	        em.remove(get(id));
	        em.getTransaction().commit();
	    }
	 
	    public AllUsersTable get(long id){
	    	AllUsersTable aut = new AllUsersTable();
	    	aut = em.find(AllUsersTable.class, id);
	    	dt = dtm.get(Integer.valueOf(aut.getDepartment()));
	    	aut.setDepartment(dt.getNameDep());
	    	pt = pts.get(Integer.valueOf(aut.getPosition()));
	    	aut.setPosition(pt.getPositionName());
	        return aut;
	    }
	 
	    public void update(AllUsersTable us){
	        em.getTransaction().begin();
	        em.merge(us);
	        em.getTransaction().commit();
	    }
	 
	    public List<AllUsersTable> getAll(){
	        TypedQuery<AllUsersTable> namedQuery = em.createNamedQuery("User.getAll", AllUsersTable.class);
	        return namedQuery.getResultList();
	    }
	 
	}