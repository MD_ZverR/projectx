package UserDbManager;

	import java.util.List;

	import javax.persistence.EntityManager;
	import javax.persistence.Persistence;
import javax.persistence.PersistenceUnit;
import javax.persistence.TypedQuery;

import UserDbEntities.DepTable;


	public class DepManager {

		@PersistenceUnit(name = "UsersDataBase")
	    public EntityManager em = Persistence.createEntityManagerFactory("UsersDataBase").createEntityManager();
		 
	    public DepTable add(DepTable dt){
	        em.getTransaction().begin();
	        DepTable dFromDB = em.merge(dt);
	        em.getTransaction().commit();
	        return dFromDB;
	    }
	 
	    public void delete(long id){
	        em.getTransaction().begin();
	        em.remove(get(id));
	        em.getTransaction().commit();
	    }
	 
	    public DepTable get(long id){
	        return em.find(DepTable.class, id);
	    }
	 
	    public void update(DepTable dt){
	        em.getTransaction().begin();
	        em.merge(dt);
	        em.getTransaction().commit();
	    }
	 
	    public List<DepTable> getAll(){
	        TypedQuery<DepTable> namedQuery = em.createNamedQuery("DepTable.getAll", DepTable.class);
	        return namedQuery.getResultList();
	    }
	 
	}
