package UserDbEntities;

	import java.io.Serializable;

	import javax.persistence.Column;
	import javax.persistence.Entity;
	import javax.persistence.Id;
	import javax.persistence.NamedQuery;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;
	@PersistenceContext(unitName = "UsersDataBase") 
	 @Entity
		@Table(name="departments")
		@NamedQuery(name="Departments.findAll", query="SELECT d FROM departments ")
		public class DepTable implements Serializable {
			private static final long serialVersionUID = 1L;
		

			@Id
			@Column(name="kod_dep")
			private String kod_dep;

			@Column(name="name_dep")
			private String name_dep;
			
			public DepTable() {
				
			}

			public String getKodDep(){
				return kod_dep;
			}
			
			public void setKodDep(String kod_dep){
				this.kod_dep = kod_dep;
			}
			
			public String getNameDep(){
				return name_dep;
			}
			
			public void setNameDep(String name_dep){
				this.name_dep = name_dep;
			}

		}
