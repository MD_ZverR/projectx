package UserDbEntities;

	import java.io.Serializable;
	import javax.persistence.*;
	
	@Entity
	@Table(name="users")
	@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
	@PersistenceContext(unitName = "UsersDataBase") 
	public class AllUsersTable implements Serializable {
		private static final long serialVersionUID = 1L;
		@PersistenceUnit(name = "UsersDataBase")
		@Id
		@Column(name="kod_sotr")
		private int kodSotr;

		@Column(name="fio")
		private String fio;
		
		@Column(name = "status")
		private String status;
		
		@Column(name = "department")
		private String department;
		
		@Column(name = "position")
		private String position;

		public AllUsersTable() {
		}

		public int getKodSotr() {
			return this.kodSotr;
		}

		public void setKodSotr(int kodSotr) {
			this.kodSotr = kodSotr;
		}

		public String getFio() {
			return this.fio;
		}

		public void setFio(String fio) {
			this.fio = fio;
		}

		public String getStatus(){
			return this.status;
		}
		
		public void setStatus(String status){
			this.status = status;
		}
		
		public String getDepartment(){
			return this.department;
		}
		
		public void setDepartment(String dep){
			this.status = dep;
		}
		
		public String getPosition(){
			return this.position;
		}
		
		public void setPosition(String position){
			this.status = position;
		}
	}