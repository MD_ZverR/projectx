package UserDbEntities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
	@Table(name="positions")
	@NamedQuery(name="Positions.findAll", query="SELECT p FROM positions ")
	public class PositionTable implements Serializable {
		private static final long serialVersionUID = 1L;

		@Id
		@Column(name="position_kod")
		private String position_kod;

		@Column(name="position_name")
		private String position_name;
		
		public PositionTable() {
			
		}

		public String getPositionKod(){
			return position_kod;
		}
		
		public void setPositionKod(String position_kod){
			this.position_kod = position_kod;
		}
		
		public String getPositionName(){
			return position_name;
		}
		
		public void setPositionName(String position_name){
			this.position_name = position_name;
		}

	}