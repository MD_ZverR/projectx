package LIbraryCinchron;

import java.util.List;

import LibEntities.Lib;
import LibEntities.User;
import LibraryManager.*;
import UserDbEntities.AllUsersTable;
import UserDbManager.*;


public class Synchron {

	private AllUsersService users;
	private UserService libraryUsers;
	private LibService library;
	private List<AllUsersTable> allUsers;
	private List<User> allLibraryUsers;
	private List<Lib> libraryData;
	
	public Synchron(){
		users = new  AllUsersService();
		libraryUsers = new UserService();
		library = new LibService();
		allUsers = users.getAll();
		allLibraryUsers = libraryUsers.getAll();
		libraryData = library.getAll();
		synchOnStart();
	}
	
	public void synchOnStart(){
		int counter = 0;
		
		//��������� �� ������ ������� ����������
		if(allLibraryUsers.size() != 0){
			for(AllUsersTable ut: allUsers){
				for(User u: allLibraryUsers){
					if(ut.getKodSotr() == u.getKodSotr()){
						//��������� �� �������� � ��������� ���������
						synchUpdate(ut, u.getFio());
						// ��������� ������� �� ���������
						synchOnDelete(ut);
						counter++;
					}
				}
				// ���� ���������� �� ����, �� ��������� ������
				if(counter == 0){
					libraryUsers.add(new User(ut.getKodSotr(), ut.getFio()));
				}else{
					counter=0;
				}
			}
		}else{
			//���� ������� �����, �������� ���� �����������
			for(AllUsersTable ut: allUsers){
				libraryUsers.add(new User(ut.getKodSotr(), ut.getFio()));
			}
		}
	}

	public void synchUpdate(AllUsersTable user, String libraryUser){
		
		if(user.getFio() != libraryUser){
			libraryUsers.update(new User(user.getKodSotr(), user.getFio()));
		}
	}

	public void synchOnDelete(AllUsersTable user){
		String status = "����";
		String userStatus = user.getStatus().toLowerCase().trim();
		//��������� ����� ����������
		if(userStatus.contains(status)){
			int counter = 0;
			//���� �������� ��� �����
			for	(Lib l: libraryData){
				if(String.valueOf(user.getKodSotr()) == l.getKodSotr()){
					counter++;
				}
			}
			//���� �������� ���� ���, �� ������� �� ����� ��
			if(counter==0){
				users.delete(user.getKodSotr());
				libraryUsers.delete(user.getKodSotr());
			}
			
		}
		
	}
}
