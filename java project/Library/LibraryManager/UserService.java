package LibraryManager;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import LibEntities.User;

import java.util.List;
 
public class UserService {
 
    public EntityManager em = Persistence.createEntityManagerFactory("Library").createEntityManager();
 
    public User add(User us){
        em.getTransaction().begin();
        User usFromDB = em.merge(us);
        em.getTransaction().commit();
        return usFromDB;
    }
 
    public void delete(long id){
        em.getTransaction().begin();
        em.remove(get(id));
        em.getTransaction().commit();
    }
 
    public User get(long id){
        return em.find(User.class, id);
    }
 
    public void update(User us){
        em.getTransaction().begin();
        em.merge(us);
        em.getTransaction().commit();
    }
 
    public List<User> getAll(){
        TypedQuery<User> namedQuery = em.createNamedQuery("User.getAll", User.class);
        return namedQuery.getResultList();
    }
 
}