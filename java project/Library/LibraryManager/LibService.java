package LibraryManager;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import LibEntities.Book;
import LibEntities.Lib;
import LibEntities.User;

public class LibService {
	
	    public EntityManager em = Persistence.createEntityManagerFactory("Library").createEntityManager();
	    public User us;
	    public UserService userv;
	    public Book bk;
	    public BooksService bks;
	    
	    public LibService(){
	    	us = new User();
	    	userv = new UserService();
	    }
	 
	    public Lib add(Lib lb){
	        em.getTransaction().begin();
	        Lib lFromDB = em.merge(lb);
	        em.getTransaction().commit();
	        return lFromDB;
	    }
	 
	    public void delete(long id){
	        em.getTransaction().begin();
	        em.remove(get(id));
	        em.getTransaction().commit();
	    }
	 
	    public Lib get(long id){
	    	Lib lb = new Lib();
	    	lb = em.find(Lib.class, id);
	    	us = userv.get(Integer.valueOf(lb.getKodSotr()));
	    	lb.setKodSotr(us.getFio());
	    	bk = bks.get(Integer.valueOf(lb.getBookId()));
	    	lb.setBookId(bk.getBookName());
	        return lb;
	    }
	 
	    public void update(Lib lb){
	        em.getTransaction().begin();
	        em.merge(lb);
	        em.getTransaction().commit();
	    }
	 
	    public List<Lib> getAll(){
	        TypedQuery<Lib> namedQuery = em.createNamedQuery("Lib.getAll", Lib.class);
	        return namedQuery.getResultList();
	    }
	 
	}