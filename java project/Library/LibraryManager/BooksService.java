package LibraryManager;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import LibEntities.Book;


public class BooksService {

    public EntityManager em = Persistence.createEntityManagerFactory("Library").createEntityManager();
	 
    public Book add(Book bk){
        em.getTransaction().begin();
        Book bFromDB = em.merge(bk);
        em.getTransaction().commit();
        return bFromDB;
    }
 
    public void delete(long id){
        em.getTransaction().begin();
        em.remove(get(id));
        em.getTransaction().commit();
    }
 
    public Book get(long id){
        return em.find(Book.class, id);
    }
 
    public void update(Book bk){
        em.getTransaction().begin();
        em.merge(bk);
        em.getTransaction().commit();
    }
 
    public List<Book> getAll(){
        TypedQuery<Book> namedQuery = em.createNamedQuery("Book.getAll", Book.class);
        return namedQuery.getResultList();
    }
 
}