package LibEntities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the lib database table.
 * 
 */
@Entity
@NamedQuery(name="Lib.findAll", query="SELECT l FROM Lib l")
public class Lib implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Temporal(TemporalType.DATE)
	private Date bdate;

	@Column(name="book_id")
	private String bookId;

	@Temporal(TemporalType.DATE)
	private Date gdate;

	@Column(name="kod_sotr")
	private String kodSotr;

	private float pinalty;

	public Lib() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getBdate() {
		return this.bdate;
	}

	public void setBdate(Date bdate) {
		this.bdate = bdate;
	}

	public String getBookId() {
		return this.bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public Date getGdate() {
		return this.gdate;
	}

	public void setGdate(Date gdate) {
		this.gdate = gdate;
	}

	public String getKodSotr() {
		return this.kodSotr;
	}

	public void setKodSotr(String kodSotr) {
		this.kodSotr = kodSotr;
	}

	public float getPinalty() {
		return this.pinalty;
	}

	public void setPinalty(float pinalty) {
		this.pinalty = pinalty;
	}

}