package yahoo.domain;

public class Stock {

	private String symbol;
	private double lastTrade;
	private double open;
	private double previousTrade;
	
	public Stock(String symbol, double lastTrade, double close) {
		this.symbol = symbol;
		this.lastTrade = lastTrade;
		this.open = close;
		previousTrade = 0;
	}
	
	public String getPreviousTrade(){
		String ansver = "";
		double dif = (previousTrade*(lastTrade - previousTrade))/100;
		
		ansver+= previousTrade + "  ";
		
			int counter = (int) Math.abs(dif);
			for(int i=0; i< counter;i++){
				if(previousTrade<lastTrade){
					ansver+="+";
				}else{
					ansver+="-";
				}
			}
		
		ansver += " (" + String.format("%.2f", dif) + "%)";
		
		
		return ansver;
	}
	
	public void setPreviousTrade(Double val){
		previousTrade = val;
	}
	
	public String getSymbol() {
		return symbol;
	}

	public String getLastTrade() {
		String ansver="";
		double dif = (lastTrade*(open - lastTrade))/100;
		
		ansver+= lastTrade + "  ";
		
			int counter = (int) Math.abs(dif);
			for(int i=0; i< counter;i++){
				if(lastTrade<open){
					ansver+="+";
				}else{
					ansver+="-";
				}
			}
		
		ansver += " (" + String.format("%.2f", dif) + "%)";
		return ansver;
	}

	public void setOpen(Double open){
		this.open = open;
	}
	
	public double getOpen() {
		return open;
	}

	public double getLastTradeValue(){
		return lastTrade;
	}
	
	public void setLastTradeValue(Double last){
		lastTrade = last;
	}
	
	@Override
	public String toString() {
		return symbol + " " + getPreviousTrade() + " " + getLastTrade() + "  " + open;
	}
	
	
	
}
