package   yahoo;

import java.util.ArrayList;
import java.util.Scanner;

import yahoo.web.YahooException;

public class StockMarketGui {

	private String[] STOCK_SYMBOLS = { "AAPL", "GOOG", "ORCL"};
	private Jornal jn = new Jornal();

	private StockMarketMonitor monitor;

	public StockMarketGui() {
		jn.doWrite("�������� ����");
		monitor = new StockMarketMonitor(STOCK_SYMBOLS);
	}

	public void addOrg(String symbol){
		ArrayList<String> st_symbol = new ArrayList<>();
		for(String s: STOCK_SYMBOLS){
			st_symbol.add(s);
		}
		st_symbol.add(symbol);
		STOCK_SYMBOLS = new String[st_symbol.size()];
				st_symbol.toArray(STOCK_SYMBOLS);
		
		monitor = new StockMarketMonitor(STOCK_SYMBOLS);
	}
	
	public void start() {
		Scanner sc = new Scanner(System.in);
		String input = "";
		try {
			do {
				System.out.println("�������� ��������:");
				System.out.println("r - �������� ������");
				System.out.println("i - �������� �����������");
				System.out.println("l - �������� ���");
				System.out.println("x - �����");
				input = sc.nextLine();
				if (input.equalsIgnoreCase("r")) {
					monitor.refresh();
					monitor.printData();
				}else if(input.equalsIgnoreCase("i")){
					input = sc.nextLine();
					addOrg(input);
					jn.doWrite("��������� ����� �����������");
					monitor.refresh();
					monitor.printData();
				}else if(input.equalsIgnoreCase("l")){
					jn.showLog();
				}
			} while (!input.equalsIgnoreCase("x"));
		} catch (YahooException e) {
			System.out.println(e.getMessage());
		}
		sc.close();
		jn.doWrite("�������� �����");
	}

}
