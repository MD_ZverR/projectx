package yahoo;

import java.util.ArrayList;
import java.util.List;

import yahoo.domain.Stock;
import yahoo.web.YahooException;
import yahoo.web.YahooStockReader;

public class StockMarketMonitor {

	private List<Stock> currentStocks = new ArrayList<>();
	private List<Stock> lastStocks = new ArrayList<>();
	private List<YahooStockReader> readers = new ArrayList<>();
	private Jornal jn = new Jornal();
	
	public StockMarketMonitor(String[] symbols) {
		for (String symbol: symbols) {
			readers.add(new YahooStockReader(symbol));

		}
	}
	
	public void refresh() throws YahooException {
		if(!currentStocks.isEmpty()){
				lastStocks = currentStocks;
				currentStocks.clear();
				for (YahooStockReader reader: readers) {
					currentStocks.add(reader.getQuote());
				}
				for (Stock stock: currentStocks) {
					for (Stock st: lastStocks) {
						if(st.getSymbol()==stock.getSymbol()){
							stock.setPreviousTrade(st.getLastTradeValue());
						}
				
					}
				}
		}else{
			for (YahooStockReader reader: readers) {
				currentStocks.add(reader.getQuote());
			}
		}
		jn.doWrite("�������� ������ �����������");
	}
	
	public void printData() {
		for (Stock stock: currentStocks) {
			System.out.println(stock);	
		}
		jn.doWrite("������ ����������� ���������");
	}
	
}
