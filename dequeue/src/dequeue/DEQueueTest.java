package dequeue;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class DEQueueTest {
	
	public static DEQueue<String> dq;
	
	@Before
	public  void mainData(){
	dq = new DEQueue<>();
	}
	
	@Test
	public void testPushBackOneString() {
		dq.pushBack("first");
		assertEquals(1, dq.size());
		assertEquals("first", dq.back());
	}
	
	@Test
	public void testIsEmptyAfterCreate() {
	    assertEquals(0, dq.size());
	}
	
	@Test
	public void pushFrontOneString(){
		dq.pushFront("second");
		assertEquals("second", dq.front());
	}
	
	@Test
	public void testPopMethods(){
		dq.pushFront("first");
		dq.pushBack("second");
		dq.pushBack("last");
		dq.popBack();
		dq.popFront();
		assertEquals(1, dq.size());
		assertEquals("second", dq.front());
	}

	@Test
	public void testPeekMethod(){
		dq.pushFront("first");
		dq.pushBack("second");
		dq.pushBack("last");
		assertEquals("second", dq.peek(1));
	}
	
	@Test
	public void testClearMethod(){
		dq.pushFront("first");
		dq.pushBack("second");
		dq.pushBack("last");
		dq.clear();
		assertEquals(0, dq.size());
	}
	
	
	@Test
	public void testToArrayMethod(){
		dq.pushBack("last");
		Object[] arr = new Object[1];
		assertEquals(arr.getClass(), dq.toArray().getClass());
	}
	
	// Напишите недостающие методы
	
}
