package dequeue;

import java.util.LinkedList;

public class DEQueue<T> {

	public LinkedList arr;
	
	public DEQueue() {
		arr = new LinkedList<>();
					
	}
	//протестировано
	public void pushBack(T element) {
		arr.addLast(element);
		
	}
	
	//протестировано
	public void popBack(){
		arr.removeLast();
	}
	
	//протестировано
	public void pushFront(T element){
		arr.addFirst(element);
	}
	
	//протестировано
	public void popFront(){
		arr.removeFirst();
	}
	
	//протестировано
	public Object back() {
		
		return arr.getLast();
	}
	
	//протестировано
	public Object front(){
		
		return arr.getFirst();
	}
	//протестировано
	public Object peek(int position) {
		
		return arr.get(position);
	}
	
	//протестировано
	public int size() {
		
		return arr.size();
	}
	
	//протестировано
	public void clear(){
		
		arr.clear();
	}
	
	
	public Object[] toArray(){
		
		return arr.toArray();
	}
	

public static void main(String[] args){
	DEQueue dq = new DEQueue<>();
	System.out.println(dq.toArray().getClass());
}
	
}
